'use strict';

var swaggerMongoose = require('swagger-mongoose');
var swagger = require('./api/swagger.json');
var bcrypt = require('bcrypt-nodejs');

var compiledSwagger = swaggerMongoose.compile(swagger);

// User
// Execute before each user.save() call
compiledSwagger.schemas.User.pre('save', function(callback) {
  var user = this;

  // Break out if the password hasn't changed
  if (!user.isModified('password')) return callback();

  // Password changed so we need to hash it
  bcrypt.genSalt(5, function(err, salt) {
    if (err) return callback(err);

    bcrypt.hash(user.password, salt, null, function(err, hash) {
      if (err) return callback(err);
      user.password = hash;
      callback();
    });
  });
});


module.exports = compiledSwagger.models;
