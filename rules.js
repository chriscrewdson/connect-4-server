'use strict';

function trans(row, column) {
  return (row * 7) + column;
}

// Check rows, if there are 4 or more disks of the same number - return winner number
exports.getWinnerInRows = function(board) {
  // Check rows and see if there are 4 disks of the same number
  for (var row = 0; row < 6; ++row) {
    var count = 0;
    // We will compare current element with the previous
    for (var column = 0; column < 7; ++column) {
      if (board[trans(row, column)] != 0 &&
          board[trans(row, column)] == board[trans(row, column - 1)]) {
        ++count;
      } else {
        count = 1;
      }
      //DEBUG:
      // console.log('(' + row + ',' + column + '): ' +
      //   board[trans(row, column)] +
      //   ' count: ' + count);

      // Check if there are 4 in a row.
      if (count >= 4) {
        var winner = board[trans(row, column)];
        //DEBUG:
        // console.log('Player ' + winner + ' is winner ' + 
        //   'in row ' + row + ' from column ' + column + ' left');
        return winner;
      }
    }
  }
  // Otherwise return 0, which means nobody won in rows.
  return 0;
}

// Check columns, if there are 4 or more disks of the same number - return winner number
exports.getWinnerInColumns = function(board) {
  // Check rows and see if there are 4 disks of the same number
  for (var column = 0; column < 7; ++column) {
    var count = 0;
    // We will compare current element with the previous
    for (var row = 0; row < 6; ++row) {
      if (board[trans(row, column)] != 0 &&
        board[trans(row, column)] == board[trans(row - 1, column)]) {
        ++count;
      } else {
        count = 1;
      }

      //DEBUG:
      // console.log('(' + row + ',' + column + '): ' +
      //   board[trans(row, column)] +
      //   ' count: ' + count);

      // Check if there are 4 in a column.
      if (count >= 4) {
        //DEBUG:
        // console.log('Player ' + board[trans(row, column)] + ' is winner ' + 
        //   'in column ' + column + ' from row ' + row + ' up');
        return board[trans(row, column)];
      }
    }
  }
  // Otherwise return 0, which means nobody won in rows.
  return 0;
}

// Check diagonals, if there are 4 or more disks of the same number - return winner number
exports.getWinnerInDiagonals = function(board) {
  var ROWS = 6;
  var COLUMNS = 7;

  //DEBUG: console.log('Check diagonals...');

  // Diagonals
  for (var row = 0; row < ROWS - 3; row++) {
    for (var column = 0; column < COLUMNS - 3; column++) {
      //DEBUG:
      // console.log('down-right: (' + row + ',' + column + '): ' +
      //   board[trans(row, column)]);

      if (
        board[trans(row,     column    )] == 1 &&
        board[trans(row + 1, column + 1)] == 1 &&
        board[trans(row + 2, column + 2)] == 1 &&
        board[trans(row + 3, column + 3)] == 1) {
          return 1;
      }

      if (
        board[trans(row,     column    )] == 2 &&
        board[trans(row + 1, column + 1)] == 2 &&
        board[trans(row + 2, column + 2)] == 2 &&
        board[trans(row + 3, column + 3)] == 2) {
          return 2;
      }
    }
  }

  for (var row = 0; row < ROWS - 3; row++) {
    for (var column = COLUMNS - 1; column >= 3; column--) {
      //DEBUG:
      // console.log('down-left: (' + row + ',' + column + '): ' +
      //   board[trans(row, column)]);

      if (
        board[trans(row,     column    )] == 1 &&
        board[trans(row + 1, column - 1)] == 1 &&
        board[trans(row + 2, column - 2)] == 1 &&
        board[trans(row + 3, column - 3)] == 1) {
          return 1;
        }

      if (
        board[trans(row,     column    )] == 2 &&
        board[trans(row + 1, column - 1)] == 2 &&
        board[trans(row + 2, column - 2)] == 2 &&
        board[trans(row + 3, column - 3)] == 2) {
          return 2;
      }
    }
  }

  // Otherwise return 0, which means nobody won
  return 0;
}
