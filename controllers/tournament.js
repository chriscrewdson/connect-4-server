'use strict';

var url = require('url');
var Tournament = require('./tournamentService');

module.exports.tournamentsGET = function tournamentsGET (req, res, next) {
  Tournament.tournamentsGET(req, res, next);
};

module.exports.tournamentsPOST = function tournamentsPOST (req, res, next) {
  Tournament.tournamentsPOST(req, res, next);
};

module.exports.tournamentsTournamentIdGET = function tournamentsTournamentIdGET (req, res, next) {
  Tournament.tournamentsTournamentIdGET(req, res, next);
};
