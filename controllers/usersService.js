'use strict';

var models = require('../models');
exports.models = models;

var User = models.User;

var cleanProjection = { __v: false, _id: false, password: false };
var clean = function(toClean) {
  toClean._id = undefined;
  toClean.__v = undefined;
}

exports.usersGET = function(request, response, next) {
  /**
   * no parameters
  **/
  //DEBUG: console.log("request.swagger.params: " + JSON.stringify(request.swagger.params || {}, null, 2));

  User.find({}, cleanProjection, function (err, users) {
    if (err) {
      next(err);
    } else {
      //todo: clean out _id from players
      response.end(JSON.stringify(users || {}, null, 2));
    }
  })
}

exports.usersPOST = function(request, response, next) {
  /**
   * parameters:
  *    body (User)
  **/
  //DEBUG: console.log("request.swagger.params: " + JSON.stringify(request.swagger.params || {}, null, 2));

  //todo: ensure unique username

  //todo: ensure request.swagger.params.body.value.password is a good password (at least not empty)

  var newUser = new User({
    username: request.swagger.params.body.value.username,
    password: request.swagger.params.body.value.password
  });
  newUser.userId = newUser._id;

  newUser.save(function (err, newUser) {
    if (err) {
      next(err);
    } else {
      response.statusCode = 201;
      response.end();
    }
  })
}

exports.usersUserIdGET = function(request, response, next) {
  /**
   * parameters:
  *    userId (String)
  **/
  //DEBUG: console.log("request.swagger.params: " + JSON.stringify(request.swagger.params || {}, null, 2));
  
  User.findOne({ '_id': request.swagger.params.userId.value }, cleanProjection, function (err, foundUser) {
    if (err) {
      next(err);
    } else {
      if(!foundUser) {
        response.statusCode = 404;
        response.end(JSON.stringify({ 
          message: 'User not found with userId: ' + request.swagger.params.userId.value,
          fields: ['userId'],
          code: 404
        }, null, 2))
      } else {
        //todo: clean out _id from players
        response.end(JSON.stringify(foundUser, null, 2));
      }
    }
  })
}
