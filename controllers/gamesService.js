'use strict';

var models = require('../models');
var Game = models.Game;
var Move = models.Move;
var Player = models.Player;

var rules = require('../rules');

var cleanProjection = { __v: false, _id: false };
var clean = function(toClean) {
  toClean._id = undefined;
  toClean.__v = undefined;
}

exports.gamesGET = function(req, res, next) {
  /**
   * no parameters
  **/

  Game.find({}, cleanProjection, function (err, games) {
    if (err) {
      next(err);
    } else {
      res.end(JSON.stringify(games, null, 2));
    }
  })
}

exports.gamesGameIdGET = function(req, res, next) {
  /**
  * parameters:
  *   gameId (String)
  **/

  Game.findOne({
    '_id': req.swagger.params.gameId.value
  }, cleanProjection, function (err, foundGame) {
    if (err) {
      next(err);
    } else {
      if(foundGame) {
        //todo: clean _id off of each move
        res.end(JSON.stringify(foundGame, null, 2));
      } else {
        res.statusCode = 404;
        res.end(JSON.stringify({ 
          message: 'Game not found with gameId: ' + req.swagger.params.gameId.value,
          fields: ['gameId'],
          code: 404
        }, null, 2))
      }
    }
  })
}

exports.gamesPOST = function(req, res, next) {
  /**
   * parameters:
   *   body (Game)
  **/
  //DEBUG: console.log("req.swagger.params: " + JSON.stringify(req.swagger.params || {}, null, 2));

  var body = req.swagger.params.body;

  // Validate player1Id and player2Id are players
  Player.find({
    _id: { $in: [body.value.player1Id, body.value.player2Id] }
  }).
  limit(2).
  select({ playerId: 1 }).
  exec(function (err, foundPlayers) {
    if (err) {
      next(err);
    } else {
      
      //DEBUG: console.log("foundPlayers: " + JSON.stringify(foundPlayers || {}, null, 2));

      if(foundPlayers.length == 2) {

        // Validate player1Id or player2Id belongs to auth'd user
        var userOwnsPlayer = req.user.players.some(player => 
            player.playerId == body.value.player1Id ||
            player.playerId == body.value.player2Id
          );

        if (!userOwnsPlayer) {
          res.statusCode = 400;
          //todo: make this error message specify which player is unowned
          res.end(JSON.stringify({ 
            message: 'Game cannot be created with at least one player owned by authenticated user: ' + 
              body.value.player1Id + ' or ' + 
              body.value.player2Id,
            fields: ['player1Id', 'player2Id'],
            code: 400
          }, null, 2))
        } else {
          var newGame = createNewGame(body.value.player1Id, body.value.player2Id);
          newGame.save(function (err, newGame) {
            if (err) {
              next(err);
            } else {
              clean(newGame);
              res.setHeader('Content-Type', 'application/json');
              res.statusCode = 201;
              res.end(JSON.stringify(newGame || {}, null, 2));
            }
          })
        }
      } else {
        res.statusCode = 400;
        //todo: make this error message specify which player is nonexistent
        res.end(JSON.stringify({ 
          message: 'Game cannot be created with nonexistent player: ' + 
            body.value.player1Id + ' or ' + 
            body.value.player2Id,
          fields: ['player1Id', 'player2Id'],
          code: 400
        }, null, 2))
      }
    }
  });
}

function createNewGame(player1Id, player2Id) {
  var newGame = new Game({
    player1Id: player1Id,
    player2Id: player2Id,
    board: [
      0,0,0,0,0,0,0,
      0,0,0,0,0,0,0,
      0,0,0,0,0,0,0,
      0,0,0,0,0,0,0,
      0,0,0,0,0,0,0,
      0,0,0,0,0,0,0
    ],
    moves: [],
    playerToPlay: player1Id
  });
  newGame.gameId = newGame._id;
  return newGame;
}

exports.gamesGameIdMovesGET = function(req, res, next) {
  /**
  * parameters:
  *   gameId (String)
  **/
  
  Game.findOne({ '_id': req.swagger.params.gameId.value }, cleanProjection, function (err, foundGame) {
    if (err) {
      next(err);
    } else {
      
      if(foundGame) {
        //todo: clean _id off of each move
        res.end(JSON.stringify(foundGame.moves, null, 2));
      } else {
        res.statusCode = 404;
        res.end(JSON.stringify({ 
          message: 'Game not found with gameId: ' + req.swagger.params.gameId.value,
          fields: ['gameId'],
          code: 404
        }, null, 2))
      }
    }
  })
}

exports.validatePlayerOfMoveOwnedByUser = function(req, res, next) {
  var body = req.swagger.params.body.value;
  var gameId = req.swagger.params.gameId.value

  // Validate body.playerId is owned by auth'd user
  if(!req.user.players.id(body.playerId)){
    res.statusCode = 400;
    res.end(JSON.stringify({ 
      message: 'Cannot make a move for a player not owned by authenticated user: ' + body.playerId,
      fields: ['playerId'],
      code: 400
    }, null, 2))
  } else {
    next();
  }
}

exports.gamesGameIdMovesPOST = function(req, res, next) {
  /**
  * parameters:
  *   gameId (String)
  *   body (Game)
  **/
  //DEBUG: console.log("req.swagger.params: " + JSON.stringify(req.swagger.params || {}, null, 2));

  var body = req.swagger.params.body.value;
  var gameId = req.swagger.params.gameId.value

  //todo: refactor to chain to reduce nesting

  Game.findOne({ '_id': gameId }, function (err, foundGame) {
    if (err) {
      next(err);
    } else {
      if(!foundGame) {
        res.statusCode = 404;
        res.end(JSON.stringify({ 
          message: 'Game not found with gameId: ' + gameId,
          fields: ['gameId'],
          code: 404
        }, null, 2))
      } else {
        // Validate game is not already won
        if(foundGame.winningPlayer){
          res.statusCode = 400;
          res.end(JSON.stringify({ 
            message: 'This game was already won by ' + foundGame.winningPlayer,
            fields: [],
            code: 400
          }, null, 2))
        } else {
          // Validate game is not draw
          if(foundGame.playerToPlay == ''){
            res.statusCode = 400;
            res.end(JSON.stringify({ 
              message: 'This game was a draw.',
              fields: [],
              code: 400
            }, null, 2))
          } else {
            // Validate foundGame.playerToPlay is playerId of move
            if(foundGame.playerToPlay != body.playerId){
              res.statusCode = 400;
              res.end(JSON.stringify({ 
                message: 'It is not this player\'s turn [' + body.playerId + '], ' + 
                  'it is [' + foundGame.playerToPlay + ']\'s turn',
                fields: ['playerId'],
                code: 400
              }, null, 2))
            } else {
              if(columnFull(foundGame.board, body.column)){
                res.statusCode = 400;
                res.end(JSON.stringify({ 
                  message: 'Invalid move, column ' + body.column + ' is full',
                  fields: ['column'],
                  code: 400
                }, null, 2))
              } else {
                var newMove = new Move({
                  column: body.column,
                  playerId: body.playerId
                })
                
                // Add the move to the game and board
                newMove.moveId = foundGame.moves.length
                foundGame.moves.push(newMove);
                var foundPlace = findPlacementInBoard(foundGame.board, body.column);
                foundGame.board[foundPlace] = body.playerId == foundGame.player1Id ? 1 : 2;
                foundGame.markModified('board');

                // Check board for win condition and set game.winningPlayer as needed
                if(boardHasAWin(foundGame.board)) {
                  console.log('Game win by ' + body.playerId);
                  foundGame.winningPlayer = body.playerId;
                  foundGame.playerToPlay = '';
                } else if (foundGame.moves.length >= 41) {
                  // Game is a draw, moves 0-41 have been played
                  foundGame.winningPlayer = '';
                  foundGame.playerToPlay = '';
                } else {
                  // If game not yet won, update playerToPlay to other player
                  foundGame.playerToPlay =
                    body.playerId == foundGame.player1Id ?
                    foundGame.player2Id :
                    foundGame.player1Id
                }
                
                foundGame.save(function (err, savedGame) {
                  if (err) {
                    next(err);
                  } else {
                    //DEBUG:
                    // console.log('foundGame:')
                    // logBoard(foundGame.board);
                    // console.log('savedGame:');
                    // logBoard(savedGame.board);
                    clean(newMove);
                    res.setHeader('Content-Type', 'application/json');
                    res.statusCode = 201;
                    res.end(JSON.stringify(savedGame.moves[savedGame.moves.length - 1], null, 2));
                  }
                })
              }
            }
          }
        }
      }
    }
  })
}

// Is the chosen column full?
function columnFull(board, column) {
  // Since first 7 are top row, this is an easy lookup
  return board[column] != 0;
}

// Update board with move
function findPlacementInBoard(board, column) {
  var foundPlace = -1;
  var row = 5;

  do {
    if(board[(row * 7) + column] == 0) {
      foundPlace = (row * 7) + column;
    } else {
      row--;
    }
  }
  while (foundPlace == -1 && row >= -1);
  
  console.log('Placing in row ' + row + ', column ' + column + ' : ' + foundPlace);
  return foundPlace;
}

function logBoard(board) {
  console.log('  col: 0 1 2 3 4 5 6');
  console.log('--------------------');
  //           row 0: 0 0 0 0 0 0 0
  //           row 1: 0 0 0 0 0 0 0
  //           row 2: 0 0 0 0 0 0 0
  //           row 3: 0 0 0 0 0 0 0
  //           row 4: 0 0 0 0 0 0 0
  //           row 5: 0 0 0 0 0 0 0
  for (var row = 0; row < 6; row++) {
    console.log('row ' + row + ': ' + board.slice(row * 7, (row * 7) + 7));
  }
}

// Check board for win condition
function boardHasAWin(board) {
  //DEBUG: console.log('Checking for a win...');
  logBoard(board);

  var winner = rules.getWinnerInRows(board)
  if(winner != 0) return winner;

  winner = rules.getWinnerInColumns(board);
  if(winner != 0) return winner;

  winner = rules.getWinnerInDiagonals(board);
  if(winner != 0) return winner;

  return 0;
}

function trans(row, column) {
  return (row * 7) + column;
}

