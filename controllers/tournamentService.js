'use strict';

var ObjectId = require('mongoose').Types.ObjectId;

var models = require('../models');
var Tournament = models.Tournament;

var cleanProjection = { __v: false, _id: false };
var clean = function(toClean) {
  toClean._id = undefined;
  toClean.__v = undefined;
}

exports.tournamentsGET = function(req, res, next) {
  /**
   * no parameters
  **/
  
  Tournament.find({}, cleanProjection, function (err, tournaments) {
    if (err) {
      next(err);
    } else {
      res.end(JSON.stringify(tournaments || {}, null, 2));
    }
  })
}

exports.tournamentsPOST = function(req, res, next) {
  /**
   * parameters:
  *    body (Tournament)
  **/
  //DEBUG: console.log("req.swagger.params: " + JSON.stringify(req.swagger.params || {}, null, 2));

  var newTournament = new Tournament({
    playerIds: req.swagger.params.body.value.playerIds,
    gameIds: []
  });
  newTournament.tournamentId = newTournament._id;

  newTournament.save(function (err, newTournament) {
    if (err) {
      next(err);
    } else {
      clean(newTournament);
      res.setHeader('Content-Type', 'application/json');
      res.statusCode = 201;
      res.end(JSON.stringify(newTournament || {}, null, 2));
    }
  })
}

exports.tournamentsTournamentIdGET = function(req, res, next) {
  /**
   * parameters:
  *    tournamentId (String)
  **/
  //DEBUG: console.log("req.swagger.params: " + JSON.stringify(req.swagger.params || {}, null, 2));
  
  Tournament.findOne({ '_id': req.swagger.params.tournamentId.value }, cleanProjection, function (err, foundTournament) {
    if (err) {
      next(err);
    } else {
      if(foundTournament) {
        res.end(JSON.stringify(foundTournament || {}, null, 2));
      } else {
        res.statusCode = 404;
        res.end(JSON.stringify({ 
          message: 'Tournament not found with tournamentId: ' + req.swagger.params.tournamentId.value,
          fields: ['tournamentId'],
          code: 404
        }, null, 2))
      }
    }
  })
}
