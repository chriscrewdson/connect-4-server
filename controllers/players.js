'use strict';

var url = require('url');
var Players = require('./playersService');

module.exports.playersGET = function playersGET (req, res, next) {
  Players.playersGET(req, res, next);
};

module.exports.playersPOST = function playersPOST (req, res, next) {
  Players.playersPOST(req, res, next);
};

module.exports.playersPlayerIdGET = function playersPlayerIdGET (req, res, next) {
  Players.playersPlayerIdGET(req, res, next);
};

module.exports.playersPlayerIdDELETE = function playersPlayerIdDELETE (req, res, next) {
  Players.playersPlayerIdDELETE(req, res, next);
};
