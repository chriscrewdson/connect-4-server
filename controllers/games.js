'use strict';

var url = require('url');
var chain = require('connect-chain');
var Games = require('./gamesService');

module.exports.gamesGET = function gamesGET (req, res, next) {
  Games.gamesGET(req, res, next);
};

module.exports.gamesGameIdGET = function gamesGameIdGET (req, res, next) {
  Games.gamesGameIdGET(req, res, next);
};

module.exports.gamesGameIdMovesGET = function gamesGameIdMovesGET (req, res, next) {
  Games.gamesGameIdMovesGET(req, res, next);
};

module.exports.gamesGameIdMovesPOST = function gamesGameIdMovesPOST (req, res, next) {
  chain(
    Games.validatePlayerOfMoveOwnedByUser,
    Games.gamesGameIdMovesPOST
  )(req, res, next);
};

module.exports.gamesPOST = function gamesPOST (req, res, next) {
  Games.gamesPOST(req, res, next);
};
