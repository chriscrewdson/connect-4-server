'use strict';

var ObjectId = require('mongoose').Types.ObjectId;

var models = require('../models');
var Player = models.Player;

var cleanProjection = { __v: false, _id: false };
var clean = function(toClean) {
  toClean._id = undefined;
  toClean.__v = undefined;
}

exports.playersGET = function(req, res, next) {
  /**
   * no parameters
  **/
  
  Player.find({}, cleanProjection, function (err, games) {
    if (err) {
      next(err);
    } else {
      res.end(JSON.stringify(games || {}, null, 2));
    }
  })
}

exports.playersPOST = function(req, res, next) {
  /**
   * parameters:
  *    body (Player)
  **/
  //DEBUG: console.log("req.user: " + JSON.stringify(req.user || {}, null, 2));
  //DEBUG: console.log("req.swagger.params.body: " + JSON.stringify(req.swagger.params.body || {}, null, 2));

  //todo: ensure unique player name

  var newPlayer = new Player({
    name: req.swagger.params.body.value.name
  });
  newPlayer.playerId = newPlayer._id;

  newPlayer.save(function (err, newPlayer) {
    if (err) {
      next(err);
    } else {
      // Assign new player to authenticated user
      req.user.players.push(newPlayer);
      req.user.save(function (err, user) {
        if (err) {
          next(err);
        } else {
          clean(newPlayer);
          res.setHeader('Content-Type', 'application/json');
          res.statusCode = 201;
          res.end(JSON.stringify(newPlayer, null, 2));
        }
      });
    }
  })
}

exports.playersPlayerIdGET = function(req, res, next) {
  /**
   * parameters:
  *    playerId (String)
  **/
  //DEBUG: console.log("req.swagger.params: " + JSON.stringify(req.swagger.params || {}, null, 2));
  
  Player.findOne({
    '_id': req.swagger.params.playerId.value 
  }, cleanProjection, function (err, foundPlayer) {
    if (err) {
      next(err);
    } else {
      if(foundPlayer) {
        res.end(JSON.stringify(foundPlayer, null, 2));
      } else {
        res.statusCode = 404;
        res.end(JSON.stringify({ 
          message: 'Player not found with playerId: ' + 
            req.swagger.params.playerId.value,
          fields: ['playerId'],
          code: 404
        }, null, 2))
      }
    }
  })
}

exports.playersPlayerIdDELETE = function(req, res, next) {
  /**
   * parameters:
  *    playerId (String)
  **/
  //DEBUG: console.log("req.swagger.params: " + JSON.stringify(req.swagger.params || {}, null, 2));

  Player.findOne({
    '_id': req.swagger.params.playerId.value
  }, {}, function (err, foundPlayer) {
    if (err) {
      next(err);
    } else {
      if(foundPlayer) {

        //DEBUG: console.log("foundPlayer: " + JSON.stringify(foundPlayer || {}, null, 2));
        //DEBUG: console.log("req.user.players: " + JSON.stringify(req.user.players || {}, null, 2));

        var userPlayers = req.user.players;
        for(var ii = userPlayers.length-1; ii--;){
          if (userPlayers[ii].playerId == foundPlayer.playerId) {
            userPlayers.splice(ii, 1)
          }
        }

        //DEBUG: console.log("spliced req.user.players: " + JSON.stringify(req.user.players || {}, null, 2));

        req.user.save(function (err) {
          if (err) {
            next(err);
          } else {
            foundPlayer.remove(function (err, removedPlayer) {
              if (err) {
                next(err);
              } else {
                res.statusCode = 204;
                res.end();
              }
            })
          }
        });
      } else {
        res.statusCode = 404;
        res.end(JSON.stringify({ 
          message: 'Player not found with playerId: ' + req.swagger.params.playerId.value,
          fields: ['gameId'],
          code: 404
        }, null, 2))
      }
    }
  })
}