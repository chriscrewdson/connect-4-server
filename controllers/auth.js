var bcrypt = require('bcrypt-nodejs');
var passport = require('passport');
var BasicStrategy = require('passport-http').BasicStrategy;

var models = require('../models');
var User = models.User;

passport.use(new BasicStrategy(
  function(username, password, callback) {
    //DEBUG: console.log('auth with u: ' + username + " p: " + password);

    User.findOne({ username: username }, function (err, user) {
      if (err) { return callback(err); }

      // No user found with that username
      if (!user) { return callback(null, false); }

      //DEBUG: console.log('found matching user: ' + JSON.stringify(user, null, 2));

      // Make sure the password is correct
      bcrypt.compare(password, user.password, function(err, isMatch) {
        if (err) { return callback(err); }

        // Password did not match
        if (!isMatch) { return callback(null, false); }

        // Success
        return callback(null, user);
      });
    });
  }
));

exports.isAuthenticated = passport.authenticate('basic', { session : false });
