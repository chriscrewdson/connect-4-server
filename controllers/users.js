'use strict';

var url = require('url');
var UsersService = require('./usersService');

module.exports.usersGET = function usersGET (req, res, next) {
  UsersService.usersGET(req, res, next);
};

module.exports.usersPOST = function usersPOST (req, res, next) {
  UsersService.usersPOST(req, res, next);
};

module.exports.usersUserIdGET = function usersUserIdGET (req, res, next) {
  UsersService.usersUserIdGET(req, res, next);
};
