'use strict';

var assert = require('assert');
var rules = require('../rules');

describe("rules", function() {
  // describe("columnFull", function() {
  //   // it("should be able to detect full columns", function(done) {
      
  //   //   done();
  //   // });

  // });

  // describe("findPlacementInBoard", function() {
  //   // it("should be able to find a placement", function(done) {
      
  //   //   done();
  //   // });

  // });

  // describe("boardHasAWin", function() {
  //   // it("should be able to detect a win", function(done) {
      
  //   //   done();
  //   // });
  // });

  describe("getWinnerInRows", function() {
    var boardsWithWinInARowTestCases = [
      {
        board: [
          0,0,0,0,0,0,0,
          0,0,0,0,0,0,0,
          0,0,0,0,0,0,0,
          0,0,0,0,0,0,0,
          0,0,0,0,0,0,0,
          0,0,0,1,1,1,1
        ],
        row: 5,
        expected: 1
      }, {
        board: [
          0,0,0,0,0,0,0,
          0,0,0,0,0,0,0,
          0,0,0,0,0,0,0,
          0,0,0,0,0,0,0,
          0,0,0,0,0,0,0,
          1,1,1,1,0,0,0
        ],
        row: 5,
        expected: 1
      }, {
        board: [
          0,0,0,0,0,0,0,
          0,0,0,0,0,0,0,
          0,0,0,0,0,0,0,
          0,0,0,0,0,0,0,
          0,0,0,0,0,0,0,
          0,2,2,2,2,0,0
        ],
        row: 5,
        expected: 2
      }, {
        board: [
          0,0,0,0,0,0,0,
          0,0,0,0,0,0,0,
          0,0,0,0,0,0,0,
          0,0,0,0,0,0,0,
          1,1,1,1,0,0,0,
          0,0,0,0,0,0,0
        ],
        row: 4,
        expected: 1
      }
    ]

    boardsWithWinInARowTestCases.forEach(function(test) {
      it("should be able to detect a win in row " + test.row, function() {
        var actual = rules.getWinnerInRows(test.board)
        assert.equal(actual, test.expected);
      });
    });
  });

  describe("getWinnerInColumns", function() {
    var boardsWithWinInAColumnTestCases = [
      {
        board: [
          0,0,0,0,0,0,0,
          0,0,0,0,0,0,0,
          1,0,0,0,0,0,0,
          1,0,0,0,0,0,0,
          1,0,0,0,0,0,0,
          1,0,0,0,0,0,0
        ],
        column: 0,
        expected: 1
      }, {
        board: [
          0,0,0,0,0,0,0,
          1,0,0,0,0,0,0,
          1,0,0,0,0,0,0,
          1,0,0,0,0,0,0,
          1,0,0,0,0,0,0,
          0,0,0,0,0,0,0
        ],
        column: 0,
        expected: 1
      }, {
        board: [
          0,0,0,0,0,0,0,
          0,0,0,0,0,0,0,
          0,2,0,0,0,0,0,
          0,2,0,0,0,0,0,
          0,2,0,0,0,0,0,
          0,2,0,0,0,0,0
        ],
        column: 1,
        expected: 2
      }, {
        board: [
          0,0,0,0,0,0,1,
          0,0,0,0,0,0,1,
          0,0,0,0,0,0,1,
          0,0,0,0,0,0,1,
          0,0,0,0,0,0,0,
          0,0,0,0,0,0,0
        ],
        column: 6,
        expected: 1
      }
    ]
    boardsWithWinInAColumnTestCases.forEach(function(test) {
      it("should be able to detect a win in column " + test.column, function() {
        var actual = rules.getWinnerInColumns(test.board)
        assert.equal(actual, test.expected);
      });
    });
  });

  describe("getWinnerInDiagonals", function() {
    var boardsWithWinInADiagonalTestCases = [
      {
        board: [
          1,0,0,0,0,0,0,
          0,1,0,0,0,0,0,
          0,0,1,0,0,0,0,
          0,0,0,1,0,0,0,
          0,0,0,0,0,0,0,
          0,0,0,0,0,0,0
        ],
        expected: 1
      }, {
        board: [
          0,0,0,0,0,0,0,
          0,0,0,0,0,0,0,
          1,0,0,0,0,0,0,
          0,1,0,0,0,0,0,
          0,0,1,0,0,0,0,
          0,0,0,1,0,0,0
        ],
        expected: 1
      }, {
        board: [
          0,0,0,1,0,0,0,
          0,0,0,0,1,0,0,
          0,0,0,0,0,1,0,
          0,0,0,0,0,0,1,
          0,0,0,0,0,0,0,
          0,0,0,0,0,0,0
        ],
        expected: 1
      }, {
        board: [
          0,0,0,2,0,0,0,
          0,0,2,0,0,0,0,
          0,2,0,0,0,0,0,
          2,0,0,0,0,0,0,
          0,0,0,0,0,0,0,
          0,0,0,0,0,0,0
        ],
        expected: 2
      }, {
        board: [
          0,0,0,0,0,0,0,
          0,0,0,0,0,0,0,
          0,0,0,0,0,0,1,
          0,0,0,0,0,1,0,
          0,0,0,0,1,0,0,
          0,0,0,1,0,0,0
        ],
        expected: 1
      }, {
        board: [
          0,0,0,0,0,0,0,
          0,0,0,0,0,0,0,
          0,0,0,0,0,1,0,
          0,0,0,0,1,0,0,
          0,0,0,1,0,0,0,
          0,0,1,0,0,0,0
        ],
        expected: 1
      }, {
        board: [
          0,0,0,0,0,0,1,
          0,0,0,0,0,1,0,
          0,0,0,0,1,0,0,
          0,0,0,1,0,0,0,
          0,0,0,0,0,0,0,
          0,0,0,0,0,0,0
        ],
        expected: 1
      }, {
        board: [
          0,0,0,0,0,0,0,
          0,0,0,0,0,0,0,
          0,0,0,0,1,0,0,
          0,0,0,1,0,0,0,
          0,0,1,0,0,0,0,
          0,1,0,0,0,0,0
        ],
        expected: 1
      }, {
        board: [
          0,0,0,0,0,0,0,
          0,0,0,0,1,0,0,
          0,0,0,1,0,0,0,
          0,0,1,0,0,0,0,
          0,1,0,0,0,0,0,
          0,0,0,0,0,0,0
        ],
        expected: 1
      }, {
        board: [
          0,0,0,0,0,0,0,
          0,2,0,0,0,0,0,
          0,0,2,0,0,0,0,
          0,0,0,2,0,0,0,
          0,0,0,0,2,0,0,
          0,0,0,0,0,0,0
        ],
        expected: 2
      }, {
        board: [
          0,0,0,0,0,0,0,
          0,0,0,0,0,0,0,
          0,0,0,2,0,0,0,
          0,0,0,0,2,0,0,
          0,0,0,0,0,2,0,
          0,0,0,0,0,0,2
        ],
        expected: 2
      }
    ]

    var ii = 0;
    boardsWithWinInADiagonalTestCases.forEach(function(testCase) {
      it('should be able to detect a win in diagonal', function() {
        //DEBUG:
        // console.log('  col: 0 1 2 3 4 5 6');
        // console.log('      --------------');
        // for (var row = 0; row < 6; row++) {
        //   console.log('row ' + row + '| ' + 
        //     testCase.board.slice(row * 7, (row * 7) + 7));
        // }
        var actual = rules.getWinnerInDiagonals(testCase.board);
        assert.equal(actual, testCase.expected);
        //DEBUG: console.log('actual: ' + actual + ' expected: ' + testCase.expected);
      });
    });
  });
});
