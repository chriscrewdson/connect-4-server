# Connect 4 Server #

### A server for games of Connect 4 ###
* v0.0.4

* Heroku hosting https://dashboard.heroku.com/apps/connect-4-server
* Deployed at https://connect-4-server.herokuapp.com/
* Object modelling http://mongoosejs.com/index.html
* Swagger middleware https://github.com/apigee-127/swagger-tools

### Basic interaction flow

* Create a user (POST /users)
* Create a player (POST /players)
* Find (GET /players) or create another player
* Create a game with those players (POST /games)
* Create moves in that game until complete (POST /games/moves)

### Running the server

```
npm install
npm run-script watch
```

To view the Swagger UI interface:

```
open http://localhost:8080/docs
```
