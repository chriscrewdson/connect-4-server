'use strict';

var app = require('connect')();
var http = require('http');
var swaggerTools = require('swagger-tools');
var mongoose = require('mongoose');
var passport = require('passport');
var authController = require('./controllers/auth');

var serverPort = process.env.PORT || process.env.npm_config_PORT || 8080;
var nodeEnv = process.env.NODE_ENV || process.env.npm_config_NODE_ENV
var mongolabUri = process.env.MONGOLAB_URI || process.env.npm_config_MONGOLAB_URI

console.log('NODE_ENV: ' + nodeEnv);

mongoose.connect(mongolabUri);
mongoose.connection.on('error', console.error.bind(console, 'connection error:'));

// swaggerRouter configuration
var options = {
  controllers: './controllers',
  useStubs: false
};

// The Swagger document (require it, build it programmatically, fetch it from a URL, ...)
var swaggerDoc = require('./api/swagger.json');
if (nodeEnv === 'development') {
  swaggerDoc.host='localhost:' + serverPort;
  swaggerDoc.schemes=["http"];
}

// Initialize the Swagger middleware
swaggerTools.initializeMiddleware(swaggerDoc, function (middleware) {
  // Interpret Swagger resources and attach metadata to request - must be first in swagger-tools middleware chain
  app.use(middleware.swaggerMetadata());

  // Provide the security handlers
  app.use(middleware.swaggerSecurity({
    basicAuth: function (req, authOrSecDef, scopesOrApiKey, callback) {

      //DEBUG: 
      // console.log(JSON.stringify({
      //   authOrSecDef: authOrSecDef,
      //   scopesOrApiKey: scopesOrApiKey
      // }, null, 2))

      // simulate Connect middleware
      var res = {
        headers: {},
        message: '',
        setHeader: function(headerName, headerValue){
          //DEBUG: console.log('setHeader with ' + headerName + ': ' + headerValue);
          this.headers[headerName] = headerValue;
        },
        end: function(endMessage) {
          this.message = endMessage;
          //DEBUG: console.log('endMessage: ' + endMessage);
          //todo: return JSON on error
          callback(this);
        },
        toString() {
          return this.message;
        }
      };
      authController.isAuthenticated(req, res, callback);
    }
  }));

  // Validate Swagger requests
  app.use(middleware.swaggerValidator());

  // Route validated requests to appropriate controller
  app.use(middleware.swaggerRouter(options));

  // Serve the Swagger documents and Swagger UI
  app.use(middleware.swaggerUi());

  //todo: abstract into general validator for ObjectIds ???
  // if (ObjectId.isValid(args.playerId.value)) {
  // 
  // } else {
  //   res.statusCode = 404;
  //   res.end(JSON.stringify({ 
  //     message: 'Player not found with playerId: ' + args.playerId.value,
  //     fields: ['playerId'],
  //     code: 404
  //   }, null, 2))
  // }

  // Start the server
  http.createServer(app).listen(serverPort, function () {
    console.log('Your server is listening on port %d (http://localhost:%d)', serverPort, serverPort);
    console.log('Swagger-ui is available on http://localhost:%d/docs', serverPort);
  });
});
